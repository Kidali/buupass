<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>myLogo</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>

<body>

    <!-- Menu drop down -->
    <div id="men_drop_down">

        <p id="cancel_menu">Close</p>

        <ul id="menu_list_drop_down">
            <li>HOME</li>
            <li>FEATURES</li>
            <li>HOW IT WORKS</li>
            <li>CONTACT US</li>
            <li>
                SIGN UP
            </li>
        </ul>
    </div>

    </div>

    <!-- Navigation -->
    <article id="navigation">

        <div class="section group">
            <div class="col span_1_of_2">
                <h1 id="logo">myLOGO</h1>
            </div>
            <div class="col span_1_of_2">
                <img id="menu_mobile" src="menu.png">
                <ul id="menu_list">
                    <li>HOME</li>
                    <li>FEATURES</li>
                    <li>HOW IT WORKS</li>
                    <li>CONTACT US</li>
                    <li>
                        <span id="sign_up">SIGN UP</span>
                    </li>
                </ul>
            </div>
        </div>

    </article>
    <!-- End of navigation -->

    <!-- Body -->
    <article id="container">
        <div class="wrapper">
            <h1 class="caption" align="center">Seamless movement of people</h1>
            <div class="btn_container">
                <div class="col span_1_of_2">
                    <button id="btn_one" class="btn_call_to_action">Take a tour</button>
                </div>
                <div class="col span_1_of_2">

                    <button id="btn_two" class="btn_call_to_action">Set up for FREE</button>

                </div>
            </div>
        </div>
    </article>
    <!-- End of body -->

    <!-- Footer -->
    <article id="footer">

        <div class="footer_top">
            <div class="section group">
                <div class="col span_1_of_4">
                    <h4 class="footer_titles">Our home</h4>
                    <ul class="footer_list">
                        <li>4th Floor, Simple Designs Centre</li>
                        <li>Pine Road</li>
                        <li>Nairobi, Kenya</li>
                    </ul>
                </div>
                <div class="col span_1_of_4">
                    <h4 class="footer_titles">Contact us</h4>
                    <ul class="footer_list">
                        <li>info@buupass.com</li>
                        <li>+254 xxx-xxx-xxx</li>
                        <li>+254 xxx-xxx-xxx</li>
                    </ul>
                </div>
                <div class="col span_1_of_4">
                    <h4 class="footer_titles">Our resources</h4>
                    <ul class="footer_list">
                        <li>Help Section</li>
                        <li>Terms of Service</li>
                        <li>Privacy Policy</li>
                    </ul>
                </div>
                <div class="col span_1_of_4">
                    <h4 class="footer_titles">Our social scene</h4>
                    <ul class="footer_list">
                        <li>Facebook</li>
                        <li>Tweet</li>
                    </ul>
                </div>
            </div>
        </div>

        <!-- copyright -->
        <div class="footer_bottom">

            <div class="section group">
                <div class="col span_1_of_2">
                    <p class="footer_text">&copy 2015 - 2018 Sample Company Limited</p>
                </div>
                <div class="col span_1_of_2">
                    <p class="footer_text footer_text_right">Proudly made in kenya</p>
                </div>
            </div>
        </div>

    </article>
    <!-- End of footer -->

    <!-- CSS -->
    <style lang="en">
        /* Google font */
        
        @import url('https://fonts.googleapis.com/css?family=Roboto+Condensed');
        /* ------------- */
        
        * {
            font-family: 'Roboto Condensed', sans-serif;
        }
        
        body,
        html {
            padding: 0px;
            margin: 0px !important;
            overflow-x: hidden;
        }
        
        #menu_mobile {
            display: none;
        }
        
        #navigation {
            width: 100%;
            min-height: 80px;
            border: thin solid #e3e3e3;
        }
        
        .nav_div {
            float: right !important;
            height: 80px;
        }
        
        #nav_div_one {
            width: 50%;
        }
        
        #nav_div_two {
            width: 40%;
        }
        
        #menu_list {
            width: 100%;
            line-height: 50px;
        }
        
        #menu_list li {
            float: left;
            list-style: none;
            margin-left: 0%;
            /* border: thin solid red; */
            width: 130px;
            /* font-size: 13px; */
            text-align: center;
        }
        
        #sign_up {
            border: 3px solid blue;
            padding: 10%;
        }
        
        #logo {
            font-size: 55px;
            line-height: 0px;
            margin-left: 5%;
        }
        
        .wrapper {
            width: 70%;
            height: 300px;
            background: #e3e3e3;
            margin: 100px auto;
            padding: 5%;
        }
        
        .caption {
            font-size: 40px;
            margin-bottom: 4%;
        }
        
        .btn_container {
            width: 30%;
            margin: 0 auto;
            height: 80px;
        }
        
        .btn_call_to_action {
            /* float: left; */
            margin-left: 5%;
            box-shadow: none !important;
            font-weight: bold;
            height: 60px;
            width: 150px;
            border: none;
            color: #fff;
        }
        
        #btn_one {
            background: purple;
        }
        
        #btn_two {
            background: lightgreen;
        }
        
        #footer {
            width: 100%;
            min-height: 300px;
            background: #383838;
        }
        
        .footer_cube {
            width: 20%;
            min-height: 300px;
            border: thin solid #fff;
            float: left;
            margin-left: 3%;
        }
        
        .footer_titles {
            color: #fff;
            margin-left: 10%
        }
        
        .footer_list li {
            list-style: none;
            color: #fff;
        }
        
        .footer_top {
            width: 80%;
            margin: 0 auto;
        }
        
        .footer_bottom {
            width: 90%;
            border-top: 2px solid #fff !important;
            margin: 10px auto;
        }
        
        .section {
            clear: both;
            padding: 0px;
            margin: 0px;
        }
        
        .col {
            display: block;
            float: left;
            margin: 1% 0 1% 1.6%;
        }
        
        .col:first-child {
            margin-left: 0;
        }
        
        .group:before,
        .group:after {
            content: "";
            display: table;
        }
        
        .group:after {
            clear: both;
        }
        
        .span_2_of_2 {
            width: 100%;
        }
        
        .span_1_of_2 {
            width: 49.2%;
        }
        
        .footer_text {
            color: #fff;
            margin-left: 5%;
            font-weight: bold;
        }
        
        .footer_text_right {
            float: right;
            margin-right: 5%;
        }
        
        .section_group {
            width: 80%;
            margin: 0 auto;
            border-top: 2px solid #fff;
        }
        
        .section {
            clear: both;
            padding: 0px;
            margin: 0px;
        }
        
        .col:first-child {
            margin-left: 0;
        }
        
        .group:after {
            clear: both;
        }
        
        .group {
            zoom: 1;
        }
        
        .span_4_of_4 {
            width: 100%;
        }
        
        .span_3_of_4 {
            width: 74.6%;
        }
        
        .span_2_of_4 {
            width: 49.2%;
        }
        
        .span_1_of_4 {
            width: 23.8%;
        }
        
        #men_drop_down {
            display: none;
            position: absolute;
            width: 100%;
            height: 100vh;
            background: #fff;
            /* margin-top : 25%; */
        }
        
        .span_3_of_3 {
            width: 100%;
        }
        
        .span_2_of_3 {
            width: 66.13%;
        }
        
        .span_1_of_3 {
            width: 32.26%;
        }
        
        #menu_list_drop_down li {
            list-style: none;
            margin-top: 7%;
        }
        
        #cancel_menu {
            font-weight: bold;
            float: right;
            margin-right: 5%;
            border: thin solid #e3e3e3;
            padding: 4%;
        }
        /* @MEDIA QUERY */
        /* ------------ */
        
        @media only screen and (max-width: 480px) {
            .col {
                margin: 1% 0 1% 0%;
            }
        }
        
        @media only screen and (max-width: 480px) {
            .span_2_of_2,
            .span_1_of_2 {
                width: 100%;
            }
            .span_1_of_4,
            .span_2_of_4,
            .span_3_of_4,
            .span_4_of_4 {
                width: 100%;
            }
            .footer_text_right {
                float: left;
            }
            .btn_call_to_action {
                float: left !important;
                margin-top: 8%;
                height: 40px;
                width: 100px;
            }
            .wrapper {
                width: 80%;
            }
            .caption {
                font-size: 35px;
            }
            #menu_list {
                display: none;
            }
            #menu_mobile {
                display: block;
                float: right;
                margin-top: -17%;
                margin-right: 15%;
                cursor: pointer;
            }
            .span_3_of_3,
            .span_2_of_3,
            .span_1_of_3 {
                width: 100%;
            }
        }
        /* END OF MEDIA QUERY */
    </style>

    <!-- SCRIPTING -->
    <script>
        $(document).ready(function() {
            $('#menu_mobile').on('click', function() {
                    $('#men_drop_down').slideDown('slow');
                    $('html, body').css({
                        overflow: 'hidden',
                        height: '100%'
                    });
                }),
                $('#cancel_menu').on('click', function() {
                    $('#men_drop_down').slideUp('slow');
                    $('html, body').css({
                        overflow: 'auto',
                        height: 'auto'
                    });
                });
        });
    </script>
</body>

</html>