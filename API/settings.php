<?php 

$endPoints = [
    [
        // https://jsonplaceholder.typicode.com/comments?postId=1
        'base_url' => "https://jsonplaceholder.typicode.com/comments",
        'method' => 'GET',
        'content_type' => null,
        'data' => [
            "postId" => "1"
        ]
    ],
    [
        'base_url' => "https://example.com",
        'method' => 'POST',
        'content_type' => 'application/x-www-form-urlencoded',
        'data' => [
            "key1" => "value1",
            "key2" => "value2"
        ]
    ],
    [
        'base_url' => "https://example2.com/path",
        'method' => 'POST',
        'content_type' => 'application/json',
        'data' => [
            "key1" => "value1",
            "key2" => "value2"
        ]
    ],
];


?>