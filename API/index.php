<?php

class Settings
{
    
    function ReturnSettings()
    {
        // API Settings
        require_once "settings.php";
        
        //  Scan through outer loop
        foreach ($endPoints as $innerArray) {
            
            $baseUrl     = $innerArray['base_url'];
            $methodType  = $innerArray['method'];
            $contentType = $innerArray['content_type'];
            
            if ($methodType == "GET") {
                
                //Run GET request
                $postId = $innerArray['data']['postId'];

                if(is_null($postId))  {
                    $newBaseUrl = baseUrl;
                } else {
                    $newBaseUrl = baseUrl.'?postId='.$postId;
                }
                
                $url     = $newBaseUrl;

                $options = array(
                    'http' => array(
                        'header' => null,
                        'method' => 'GET',
                        'content' => http_build_query(null)
                    )
                );
                
                $context = stream_context_create($options);
                $result  = file_get_contents($url, false, $context);
                echo $result;
                
            } else {
                
                // Run POST request
                $value1 = $innerArray['data']['key1'];
                $value2 = $innerArray['data']['key2'];

                $url     = $baseUrl;
                $data    = array(
                    'key1' => $value1,
                    'key2' => $value2
                );
                $options = array(
                    'http' => array(
                        'header' => $contentType,
                        'method' => 'POST',
                        'content' => http_build_query($data)
                    )
                );
                
                $context = stream_context_create($options);
                $result  = file_get_contents($url, false, $context);
                echo $result;
                
            }
            
            echo "<hr/>";
            
        }
        
        
    }
    
}
// ReturnSettings();
$settingObj = new Settings();

$settingObj->ReturnSettings();

?>